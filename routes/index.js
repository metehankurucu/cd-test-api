var express = require('express');
var router = express.Router();

const testRepo = require('../repositories/test');

router.get('/', async (req, res, next) => {
  res.send('DEPLOY APP RUNNING ' + __dirname);
});

router.post('/deploy', async (req, res, next) => {
  try {
    const { repository: { uuid, name } } = req.body;
    //Deploy operations for test repo
    if (name === testRepo.name) {
      await testRepo.deploy();
    }
    res.json({ result: true });
    console.log({ result: true })
  } catch (error) {
    res.json({
      result: false,
      error: error.message
    });
    console.log({
      result: false,
      error: error.message
    })
  }
});

module.exports = router;
