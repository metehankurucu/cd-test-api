#!/bin/bash
echo "[INFO] Deployment started"
# ssh kns@167.99.133.245 'bash -s' 
# export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")" [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
nvm install v12
cd ~/apps
cd test
if [ $? -eq 0 ]
then
  echo "[INFO] Repo exist"
  cd .git
  if [ $? -eq 0 ]
  then
    echo "[INFO] Git exist"
    cd ..
    git pull
  else
    echo "[INFO] Git does not exist, folder deleting and cloning from origin.."
    cd ..
    rm -rf test
    git clone git@bitbucket.org:metehankurucu/test.git
    cd test
  fi
else
  echo "[INFO] Repo does not exist, cloning.."
  git clone git@bitbucket.org:metehankurucu/test.git
  cd test
fi
echo "[INFO] Dependencies installing"
npm install
pm2 stop test
pm2 start ./bin/www --name test
echo "[INFO] Deployment successful, app running."
